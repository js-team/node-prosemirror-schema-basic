# Installation
> `npm install --save @types/prosemirror-schema-basic`

# Summary
This package contains type definitions for prosemirror-schema-basic (https://github.com/ProseMirror/prosemirror-schema-basic).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/prosemirror-schema-basic.

### Additional Details
 * Last updated: Mon, 18 Jan 2021 15:07:08 GMT
 * Dependencies: [@types/prosemirror-model](https://npmjs.com/package/@types/prosemirror-model)
 * Global values: none

# Credits
These definitions were written by [Bradley Ayers](https://github.com/bradleyayers), [David Hahn](https://github.com/davidka), [Tim Baumann](https://github.com/timjb), and [Patrick Simmelbauer](https://github.com/patsimm).
